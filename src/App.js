import React from "react";

import { BrowserRouter as Router, Route, Link } from "react-router-dom";
import { ThemeProvider } from "styled-components";

import UserPage from "./UserPage";
import HomePage from "./HomePage";

function App() {
  const myTheme = {
    background: "pink",
    foreground: "yellow"
  };

  return (
    <ThemeProvider theme={myTheme}>
      <Router>
        <div>
          <nav>
            <ul>
              <li>
                <Link to="/">Home</Link>
              </li>
              <li>
                <Link to="/user/">User</Link>
              </li>
            </ul>
          </nav>

          <Route path="/" exact component={HomePage} />
          <Route path="/user/" component={UserPage} />
        </div>
      </Router>
    </ThemeProvider>
  );
}

export default App;
