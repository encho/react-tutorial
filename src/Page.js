import React from "react";
import styled from "styled-components";

const PageWrapper = styled.div`
  background-color: ${props => props.theme.background};
  color: ${props => props.theme.foreground};
`;

const Page = ({ children }) => {
  return <PageWrapper>{children}</PageWrapper>;
};

export default Page;
