import React from "react";
import Page from "./Page";

const UserDetails = ({ name }) => {
  return <h2>Vorname: {name}</h2>;
};

const UserPage = () => {
  const nameFromInputField = "Mouse";
  return (
    <Page>
      <h1>User Page</h1>
      <UserDetails name={nameFromInputField} />
    </Page>
  );
};

export default UserPage;
