import React from "react";
import Page from "./Page";

const HomePage = () => {
  return (
    <Page>
      <h1>Home Page</h1>
    </Page>
  );
};

export default HomePage;
